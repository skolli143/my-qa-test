Feature: Wikipedia Search
  As I customer I can able to search Wikipedia
  So that I can navigate to searched pages

  @searchWiki
  Scenario: Wikipedia search for specific information
    Given I navigate to Wikipedia home page
    When I type a keyword "European Union"  in the search input field
    And select "English" as a search language
    And click on search button
    Then I navigate "European Union" "English" wiki page
    When I change the language to "Deutsch"
    Then I navigate "Europäische Union" "Deutsch" wiki page
    When I change the language to "English"
    Then I navigate "European Union" "English" wiki page