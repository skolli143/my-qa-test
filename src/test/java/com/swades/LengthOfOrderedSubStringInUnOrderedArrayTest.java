package com.swades;

import static org.junit.Assert.*;
import org.junit.Test;

import static com.swades.LengthOfOrderedSubStringInUnOrderedArray.iLengthOfOrderedSubStringInUnOrderedArray;

public class LengthOfOrderedSubStringInUnOrderedArrayTest {

    @Test
    public void iVerifyLengthOfOrderedSubStringInUnOrderedArray(){
        int arr[] = {1, 4, 1, 4, 2, 1, 3, 5, 6, 2, 3, 7};
        int arr1[] = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};
        int arr2[] = {2, 7, 1, 8, 2, 8, 1};

        assertEquals(4,iLengthOfOrderedSubStringInUnOrderedArray(arr));
        assertEquals(3,iLengthOfOrderedSubStringInUnOrderedArray(arr1));
        assertEquals(2,iLengthOfOrderedSubStringInUnOrderedArray(arr2));

        System.out.println("Verify length of ordered sub string in Unordered Array PASSED");

    }
}
