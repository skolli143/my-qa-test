package com.swades;

import static org.junit.Assert.*;

import com.swades.service.Calculator;
import org.junit.Test;

public class CalculatorWebServiceClientTest {

    @Test
    public void addTest(){
        assertEquals(30,new Calculator().getCalculatorSoap().add(10,20));
    }

    @Test
    public void subtractTest(){
        assertEquals(10,new Calculator().getCalculatorSoap().subtract(20,10));
    }

    @Test
    public void multiplicationTest(){
        assertEquals(200,new Calculator().getCalculatorSoap().multiply(10,20));
    }

    @Test
    public void divisionTest(){
        assertEquals(5,new Calculator().getCalculatorSoap().divide(100,20));
    }
}
