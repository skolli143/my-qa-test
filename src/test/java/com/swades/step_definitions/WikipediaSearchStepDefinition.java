package com.swades.step_definitions;

import com.swades.page_objects.WikipediaHomePage;
import cucumber.api.java.After;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

import java.io.IOException;

public class WikipediaSearchStepDefinition {

    WikipediaHomePage wikipediaHomePage;

    public WikipediaSearchStepDefinition() throws IOException {
        wikipediaHomePage = new WikipediaHomePage();
    }

    @Given("^I navigate to Wikipedia home page$")
    public void i_navigate_to_Wikipedia_home_page() {
        wikipediaHomePage.navigateToWikiHomePage();
    }

    @When("^I type a keyword \"([^\"]*)\"  in the search input field$")
    public void i_type_a_keyword_in_the_search_input_field(String searchKeyWord) {
        wikipediaHomePage.enterGivenKeyWordIntoSearchInput(searchKeyWord);
    }

    @And("^select \"([^\"]*)\" as a search language$")
    public void select_as_a_search_language(String language){
        wikipediaHomePage.selectLanguageFromDropDown(language);
    }

    @And("^click on search button$")
    public void click_on_search_button(){
        wikipediaHomePage.clickSearchButton();
    }

    @Then("^I navigate \"([^\"]*)\" \"([^\"]*)\" wiki page$")
    public void i_navigate_European_Union_wiki_page(String pageHeading, String language) throws Exception {
        Assert.assertEquals(pageHeading, wikipediaHomePage.getPageHeading());
    }

    @When("^I change the language to \"([^\"]*)\"$")
    public void i_change_the_language_to(String language) throws InterruptedException {
        wikipediaHomePage.clickOnGivenLanguageLink(language);
    }

    @After
    public void teardown(){
        wikipediaHomePage.tearDown();
    }

}
