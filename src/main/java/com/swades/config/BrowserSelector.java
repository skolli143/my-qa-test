package com.swades.config;

import org.openqa.selenium.NotFoundException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

public class BrowserSelector {

    private String BROWSER = System.getProperty("browser");
    private String OS = System.getProperty("os.name");
    private WebDriver driver;

    public WebDriver getDriver() throws MalformedURLException {
        if (driver == null) {
            if (BROWSER == null) {
                setChromeDriver();
                driver = new ChromeDriver();
            } else {
                switch (BROWSER) {
                    case "firefox":
                        setFirefoxDriver();
                        driver = new FirefoxDriver();
                        break;
                    case "chrome":
                        setChromeDriver();
                        driver = new ChromeDriver();
                        break;
                    case "grid-firefox":
                        String nodeUrl = "http://localhost:4444/wd/hub";
                        DesiredCapabilities desiredCapabilities = DesiredCapabilities.firefox();
                        desiredCapabilities.setBrowserName("firefox");
                        desiredCapabilities.setPlatform(Platform.LINUX);
                        driver = new RemoteWebDriver(new URL(nodeUrl), desiredCapabilities);
                    default:
                        throw new NotFoundException();
                }
            }
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        }
        return driver;
    }

    private void setChromeDriver() {
        switch (OS) {
            case "Mac OS X":
                System.setProperty("webdriver.chrome.driver", "drivers/mac/chromedriver");
                break;
            case "Linux":
                System.setProperty("webdriver.chrome.driver", "drivers/linux/chromedriver");
                break;
            default:
                System.setProperty("webdriver.chrome.driver", "drivers/windows/chromedriver.exe");
                break;
        }
    }

    private void setFirefoxDriver() {
        switch (OS) {
            case "Mac OS X":
                System.setProperty("webdriver.gecko.driver", "drivers/mac/geckodriver");
                break;
            case "Linux":
                System.setProperty("webdriver.gecko.driver", "sdrivers/linux/geckodriver");
                break;
            default:
                System.setProperty("webdriver.gecko.driver", "drivers/windows/geckodriver");
                break;
        }
    }

}
