package com.swades.config;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class TestProperties {

    private static final String CONFIG_PATH = "properties/";
    private static final String ENV_KEY = "environment";

    private static final Logger LOGGER = LoggerFactory.getLogger(TestProperties.class);

    private Properties properties = new Properties();

    public TestProperties() throws IOException {
        String filePath = System.getProperty(ENV_KEY) == null ?
                CONFIG_PATH + "env.properties" :
                CONFIG_PATH + "env." + System.getProperty(ENV_KEY) + ".properties";

        LOGGER.info("file path {}"+filePath);

        InputStream inputStream = getClass().getClassLoader().getResourceAsStream(filePath);

        properties.load(inputStream);

    }

    public String getProperty(String PropertyName) {
        return properties.getProperty(PropertyName);
    }

}
