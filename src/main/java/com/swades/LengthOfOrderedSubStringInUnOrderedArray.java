package com.swades;

public class LengthOfOrderedSubStringInUnOrderedArray {

//    public static void main(String[] args) {
//
//        int arr[] = {1, 4, 1, 4, 2, 1, 3, 5, 6, 2, 3, 7};
//        int arr1[] = {3, 1, 4, 1, 5, 9, 2, 6, 5, 3, 5};
//        int arr2[] = {2, 7, 1, 8, 2, 8, 1};
//
//        System.out.println("Longest Length of ordered substring : " + iLengthOfOrderedSubStringInUnOrderedArray(arr));
//        System.out.println("Longest Length of ordered substring : " + iLengthOfOrderedSubStringInUnOrderedArray(arr1));
//        System.out.println("Longest Length of ordered substring : " + iLengthOfOrderedSubStringInUnOrderedArray(arr2));
//
//    }

    /**
     *
     * @param arr
     * @returns maximum length of ordered substring in an unordered string
     */
    public static int iLengthOfOrderedSubStringInUnOrderedArray(int arr[]) {

        int longestLength = 1;
        int max = 0;

        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i] < arr[i + 1]) {
                longestLength++;
            } else {
                if (max < longestLength) {
                    max = longestLength;
                }
                longestLength = 1;
            }
        }
        return max;
    }
}
