package com.swades.page_objects;

import com.swades.config.BrowserSelector;
import com.swades.config.TestProperties;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

public class BasePage {

    public WebDriver driver;
    public TestProperties testProperties;

    public BasePage() throws IOException {
        BrowserSelector browserSelector = new BrowserSelector();
        testProperties = new TestProperties();
        this.driver = browserSelector.getDriver();
    }

    public void tearDown(){
        driver.quit();
    }
}
