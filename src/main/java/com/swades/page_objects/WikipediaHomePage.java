package com.swades.page_objects;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.Select;

import java.io.IOException;

public class WikipediaHomePage extends BasePage{

    private final String txtSearchInput = "searchInput";
    private final String btnSearchButton = "//*[@id=\"search-form\"]/fieldset/button/i";
    private final String drpLanguage = "searchLanguage";

    public WikipediaHomePage() throws IOException {
    }


    public void navigateToWikiHomePage() {
        driver.get(testProperties.getProperty("WikipediaHomePage"));
    }

    public void enterGivenKeyWordIntoSearchInput(String input) {
        driver.findElement(By.id(txtSearchInput)).sendKeys(input);
    }

    public void clickSearchButton() {
        driver.findElement(By.xpath(btnSearchButton)).click();
    }

    public void selectLanguageFromDropDown(String input) {
        Select language = new Select(driver.findElement(By.id(drpLanguage)));

        switch (input) {
            case "English":
                language.selectByVisibleText("English");
                break;
            case "Polish":
                language.selectByVisibleText("Polski");
                break;
        }
    }

    private final String wikiResultPageHeading = "firstHeading";

    public String getPageHeading(){
        return driver.findElement(By.id(wikiResultPageHeading)).getText();
    }

    public void clickOnGivenLanguageLink(String lnkText) {
        driver.findElement(By.partialLinkText(lnkText)).click();
    }

}
