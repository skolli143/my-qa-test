1 . To run Given an unordered array of integers of length N > 0, calculate the length of the
         longest ordered (ascending from left [lower index] to right [higher index])
         subsequence within the array.
- mvn clean test -Dtest=com.swades.LengthOfOrderedSubStringInUnOrderedArrayTest

2 . Web Front-End Test
- mvn clean test -Dtest=com.swades.runners.RunCukesTest

3 . Web Service Test
-  mvn clean test -Dtest=com.swades.CalculatorWebServiceClientTest